"""This file contains all the classes you must complete for this project.

You can use the test cases in agent_test.py to help during development, and
augment the test suite with your own test cases to further test your code.

You must test your agent's strength against a set of agents with known
relative strength using tournament.py and include the results in your report.
"""
import random
import isolation


class Timeout(Exception):
    """Subclass base exception for code clarity."""
    pass


# night player's relative movement
DIRECTIONS = [(-2, -1), (-1, -2), (1, -2), (2, -1), (2, 1), (1, 2), (-1, 2), (-2, 1)]
# when to change phase from the first to the second
SWITCHING_STEPS_TO_SECOND = 25


def custom_score_movable_num_inner_two_steps(game, myplayer):
    """
    check number of movable places inner two steps for each players.

    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).

    myplayer : object
        A player instance in the current game (i.e., an object corresponding to
        one of the player objects `game.__player_1__` or `game.__player_2__`.)

    Returns
    -------
    float
        The heuristic value of the current game state to the specified player.
    """
    if game.is_loser(myplayer):
        return float("-inf")

    if game.is_winner(myplayer):
        return float("inf")

    player_stepable_nums = []
    players = [myplayer, game.get_opponent(myplayer)]
    for player in players:
        checked_positions = []
        unchecked_positions = [game.get_player_location(player)]
        # loop twice
        for _ in range(2):
            new_available_positions = []
            # detect available and not checked positions
            for position in unchecked_positions:
                for direction in DIRECTIONS:
                    # sum each element of position and direction
                    move = tuple(map(sum, zip(position, direction)))
                    # detect whether available if not check yet
                    if move not in checked_positions:
                        if game.move_is_legal(move):
                            new_available_positions.append(move)

            # update checking states
            checked_positions += unchecked_positions
            unchecked_positions = new_available_positions
            # check converge
            if len(new_available_positions) == 0:
                break

        player_stepable_nums.append(float(len(checked_positions)))

    # return number of stepable positions for player
    return (player_stepable_nums[0] - player_stepable_nums[1]) * 10


def _score_sticky(game, myplayer):
    """
    The base evaluation is similar to custom_score_movable_num_inner_two_steps.
    The nearer distance to the adversary player is nearer, the more weighted.

    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).

    myplayer : object
        A player instance in the current game (i.e., an object corresponding to
        one of the player objects `game.__player_1__` or `game.__player_2__`.)

    Returns
    -------
    float
        The heuristic value of the current game state to the specified player.
    """
    if game.is_loser(myplayer):
        return float("-inf")

    if game.is_winner(myplayer):
        return float("inf")

    player_stepable_nums = []
    players = [myplayer, game.get_opponent(myplayer)]
    for player in players:
        checked_positions = []
        unchecked_positions = [game.get_player_location(player)]
        # loop twice
        for _ in range(1):
            new_available_positions = []
            # detect available and not checked positions
            for position in unchecked_positions:
                for direction in DIRECTIONS:
                    # sum each element of position and direction
                    move = tuple(map(sum, zip(position, direction)))
                    # detect whether available if not check yet
                    if move not in checked_positions:
                        if game.move_is_legal(move):
                            new_available_positions.append(move)

            # update checking states
            checked_positions += unchecked_positions
            unchecked_positions = new_available_positions
            # check converge
            if len(new_available_positions) == 0:
                break

        player_stepable_nums.append(float(len(checked_positions)))

    # make difference of movable positions for each player.
    difference = player_stepable_nums[0] - player_stepable_nums[1]
    # get distance to adversary player
    myplayer_position = game.get_player_location(myplayer)
    adversary_position = game.get_player_location(game.get_opponent(myplayer))
    distance = abs(myplayer_position[0] - adversary_position[0]) + \
               abs(myplayer_position[1] - adversary_position[1])
    # the near position, the more weighted
    distance_score = (game.height + game.width - distance)
    return difference * 10 + distance_score


def _score_movable_num(game, myplayer):
    """
    check number of all movable places in the future at the evaluation time for each players.

    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).

    myplayer : object
        A player instance in the current game (i.e., an object corresponding to
        one of the player objects `game.__player_1__` or `game.__player_2__`.)

    Returns
    -------
    float
        The heuristic value of the current game state to the specified player.
    """
    if game.is_loser(myplayer):
        return float("-inf")

    if game.is_winner(myplayer):
        return float("inf")

    player_movable_nums = []
    players = [myplayer, game.get_opponent(myplayer)]
    players_checked_positions = []
    for player in players:
        checked_positions = []
        unchecked_positions = [game.get_player_location(player)]
        while True:
            new_available_positions = []
            # detect available and not checked positions
            for position in unchecked_positions:
                for direction in DIRECTIONS:
                    # sum each element of position and direction
                    move = tuple(map(sum, zip(position, direction)))
                    # detect whether available if not check yet
                    if move not in checked_positions:
                        if game.move_is_legal(move):
                            new_available_positions.append(move)

            # update checking states
            checked_positions += unchecked_positions
            unchecked_positions = new_available_positions
            # check converge
            if len(new_available_positions) == 0:
                break
        # set detect result for each player
        players_checked_positions.append(checked_positions)
        player_movable_nums.append(float(len(checked_positions)))

    # return number of movable positions for player
    return (player_movable_nums[0] - player_movable_nums[1]) * 10


def _score_movable_num_with_split_check(game, myplayer):
    """
    check number of all movable places in the future at the evaluation time for each players.
    After the search, check whether split occurs.

    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).

    myplayer : object
        A player instance in the current game (i.e., an object corresponding to
        one of the player objects `game.__player_1__` or `game.__player_2__`.)

    Returns
    -------
    float
        The heuristic value of the current game state to the specified player.
    """
    if game.is_loser(myplayer):
        return float("-inf")

    if game.is_winner(myplayer):
        return float("inf")

    player_movable_nums = []
    players = [myplayer, game.get_opponent(myplayer)]
    players_checked_positions = []
    for player in players:
        checked_positions = []
        unchecked_positions = [game.get_player_location(player)]
        while True:
            new_available_positions = []
            # detect available and not checked positions
            for position in unchecked_positions:
                for direction in DIRECTIONS:
                    # sum each element of position and direction
                    move = tuple(map(sum, zip(position, direction)))
                    # detect whether available if not check yet
                    if move not in checked_positions:
                        if game.move_is_legal(move):
                            new_available_positions.append(move)

            # update checking states
            checked_positions += unchecked_positions
            unchecked_positions = new_available_positions
            # check converge
            if len(new_available_positions) == 0:
                break
        # set detect result for each player
        players_checked_positions.append(checked_positions)
        player_movable_nums.append(float(len(checked_positions)))

    if len(set(players_checked_positions[0]).intersection(set(players_checked_positions[1]))) == 0:
        # split occurs, then the player whose available space is larger will necessarily win.
        # so weighted the result
        return 100 * (player_movable_nums[0] - player_movable_nums[1])

    # return number of movable positions for player
    return (player_movable_nums[0] - player_movable_nums[1]) * 10


def custom_score_movable_num(game, player):
    """In first phase, evaluate by custom_score_movable_num_inner_two_steps.
    In last phase(last 30 steps), evaluate by _score_movable_num_until_converge.

    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).

    player : object
        A player instance in the current game (i.e., an object corresponding to
        one of the player objects `game.__player_1__` or `game.__player_2__`.)

    Returns
    -------
    float
        The heuristic value of the current game state to the specified player.
    """
    if game.is_loser(player):
        return float("-inf")

    if game.is_winner(player):
        return float("inf")

    if len(game.get_blank_spaces()) < SWITCHING_STEPS_TO_SECOND:
        return _score_movable_num(game, player)
    else:
        return custom_score_movable_num_inner_two_steps(game, player)


def custom_score_movable_num_with_split_check(game, player):
    """In first phase, evaluate by custom_score_movable_num_inner_two_steps.
    In last phase(last 30 steps), evaluate by _score_movable_num_until_converge.

    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).

    player : object
        A player instance in the current game (i.e., an object corresponding to
        one of the player objects `game.__player_1__` or `game.__player_2__`.)

    Returns
    -------
    float
        The heuristic value of the current game state to the specified player.
    """
    if game.is_loser(player):
        return float("-inf")

    if game.is_winner(player):
        return float("inf")

    if len(game.get_blank_spaces()) < (game.height * game.width / 2):
        return _score_movable_num_with_split_check(game, player)
    else:
        return custom_score_movable_num_inner_two_steps(game, player)


def custom_score_movable_num_with_split_check_after_sticky(game, player):
    """In first phase, evaluate by custom_score_movable_num_inner_two_steps.
    In last phase(last 30 steps), evaluate by _score_movable_num_until_converge.

    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).

    player : object
        A player instance in the current game (i.e., an object corresponding to
        one of the player objects `game.__player_1__` or `game.__player_2__`.)

    Returns
    -------
    float
        The heuristic value of the current game state to the specified player.
    """
    if game.is_loser(player):
        return float("-inf")

    if game.is_winner(player):
        return float("inf")

    if len(game.get_blank_spaces()) < SWITCHING_STEPS_TO_SECOND:
        return _score_movable_num_with_split_check(game, player)
    else:
        return _score_sticky(game, player)


# set custom_score function
custom_score = custom_score_movable_num_with_split_check_after_sticky


class CustomPlayer:
    """Game-playing agent that chooses a move using your evaluation function
    and a depth-limited minimax algorithm with alpha-beta pruning. You must
    finish and test this player to make sure it properly uses minimax and
    alpha-beta to return a good move before the search time limit expires.

    Parameters
    ----------
    search_depth : int (optional)
        A strictly positive integer (i.e., 1, 2, 3,...) for the number of
        layers in the game tree to explore for fixed-depth search. (i.e., a
        depth of one (1) would only explore the immediate sucessors of the
        current state.)

    score_fn : callable (optional)
        A function to use for heuristic evaluation of game states.

    iterative : boolean (optional)
        Flag indicating whether to perform fixed-depth search (False) or
        iterative deepening search (True).

    method : {'minimax', 'alphabeta'} (optional)
        The name of the search method to use in get_move().

    timeout : float (optional)
        Time remaining (in milliseconds) when search is aborted. Should be a
        positive value large enough to allow the function to return before the
        timer expires.
    """

    def __init__(self, search_depth=3, score_fn=custom_score,
                 iterative=True, method='alphabeta', timeout=10.):
        self.search_depth = search_depth
        self.iterative = iterative
        self.score = score_fn
        self.method = method
        self.time_left = None
        self.TIMER_THRESHOLD = timeout

    def get_move(self, game, legal_moves, time_left):
        """Search for the best move from the available legal moves and return a
        result before the time limit expires.

        This function must perform iterative deepening if self.iterative=True,
        and it must use the search method (minimax or alphabeta) corresponding
        to the self.method value.

        **********************************************************************
        NOTE: If time_left < 0 when this function returns, the agent will
              forfeit the game due to timeout. You must return _before_ the
              timer reaches 0.
        **********************************************************************

        Parameters
        ----------
        game : `isolation.Board`
            An instance of `isolation.Board` encoding the current state of the
            game (e.g., player locations and blocked cells).

        legal_moves : list<(int, int)>
            A list containing legal moves. Moves are encoded as tuples of pairs
            of ints defining the next (row, col) for the agent to occupy.

        time_left : callable
            A function that returns the number of milliseconds left in the
            current turn. Returning with any less than 0 ms remaining forfeits
            the game.

        Returns
        -------
        (int, int)
            Board coordinates corresponding to a legal move; may return
            (-1, -1) if there are no available legal moves.
        """

        self.time_left = time_left

        # Perform any required initializations, including selecting an initial
        # move from the game board (i.e., an opening book), or returning
        # immediately if there are no legal moves

        # if not iterative search, return search_depth search result.
        if not self.iterative:
            if self.method == "minimax":
                score, best_move = self.minimax(game, self.search_depth, True)
                return best_move
            elif self.method == "alphabeta":
                score, best_move = self.alphabeta(game, self.search_depth, True)
                return best_move
            else:
                raise RuntimeError(f"invalid method. method={self.method}")
        depth = 0
        try:
            # The search method call (alpha beta or minimax) should happen in
            # here in order to avoid timeout. The try/except block will
            # automatically catch the exception raised by the search method
            # when the timer gets close to expiring
            best_move = (-1, -1)
            max_score = float("-inf")
            while depth <= len(game.get_blank_spaces()):
                if self.method == "minimax":
                    score, move = self.minimax(game, depth, True)
                elif self.method == "alphabeta":
                    score, move = self.alphabeta(game, depth, True)
                else:
                    raise RuntimeError(f"invalid method. method={self.method}")
                if score > max_score:
                    best_move = move
                depth += 1
        except Timeout as e:
            # Handle any actions required at timeout, if necessary
            pass

        return best_move

    def minimax(self, game, depth, maximizing_player=True):
        """Implement the minimax search algorithm as described in the lectures.

        Parameters
        ----------
        game : isolation.Board
            An instance of the Isolation game `Board` class representing the
            current game state

        depth : int
            Depth is an integer representing the maximum number of plies to
            search in the game tree before aborting

        maximizing_player : bool
            Flag indicating whether the current search depth corresponds to a
            maximizing layer (True) or a minimizing layer (False)

        Returns
        -------
        float
            The score for the current search branch

        tuple(int, int)
            The best move for the current branch; (-1, -1) for no legal moves

        Notes
        -----
            (1) You MUST use the `self.score()` method for board evaluation
                to pass the project unit tests; you cannot call any other
                evaluation function directly.
        """
        if self.time_left() < self.TIMER_THRESHOLD:
            raise Timeout()

        # check whether game is over.
        if game.utility(game.active_player) != 0:
            # game is over. Then, return my player's result.
            # find my player
            if maximizing_player:
                my_player = game.active_player
            else:
                my_player = game.get_opponent(game.active_player)
            # return my player result.
            return game.utility(my_player), game.get_player_location(my_player)

        # check depth limit.
        assert depth >= 0, f"invalid depth. depth={depth}"
        if depth == 0:
            # depth reached limit. Then, return score.

            # find my player
            if maximizing_player:
                my_player = game.active_player
            else:
                my_player = game.get_opponent(game.active_player)
            # return my player current score and position.
            score = self.score(game, my_player)
            return score, game.get_player_location(my_player)

        if maximizing_player:
            # process of function MAX-VALUE
            max_score_and_move = (float("-inf"), (-1, -1))
            for legal_move in game.get_legal_moves(game.active_player):
                next_state = game.forecast_move(legal_move)
                # search minimized score when adversary player to move if search can continue.
                score, _ = self.minimax(next_state, depth - 1, not maximizing_player)
                if score > max_score_and_move[0]:
                    max_score_and_move = (score, legal_move)
            return max_score_and_move
        else:
            # process of function MIN-VALUE
            min_score_and_move = (float("inf"), (-1, -1))
            for legal_move in game.get_legal_moves(game.active_player):
                next_state = game.forecast_move(legal_move)
                # search maximized score when my player to move if search can continue.
                score, _ = self.minimax(next_state, depth - 1, not maximizing_player)
                if score < min_score_and_move[0]:
                    min_score_and_move = (score, legal_move)
            return min_score_and_move

    def alphabeta(self, game, depth, alpha=float("-inf"), beta=float("inf"), maximizing_player=True):
        """Implement minimax search with alpha-beta pruning as described in the
        lectures.

        Parameters
        ----------
        game : isolation.Board
            An instance of the Isolation game `Board` class representing the
            current game state

        depth : int
            Depth is an integer representing the maximum number of plies to
            search in the game tree before aborting

        alpha : float
            Alpha limits the lower bound of search on minimizing layers

        beta : float
            Beta limits the upper bound of search on maximizing layers

        maximizing_player : bool
            Flag indicating whether the current search depth corresponds to a
            maximizing layer (True) or a minimizing layer (False)

        Returns
        -------
        float
            The score for the current search branch

        tuple(int, int)
            The best move for the current branch; (-1, -1) for no legal moves

        Notes
        -----
            (1) You MUST use the `self.score()` method for board evaluation
                to pass the project unit tests; you cannot call any other
                evaluation function directly.
        """
        if self.time_left() < self.TIMER_THRESHOLD:
            raise Timeout()

        # check whether game is over.
        if game.utility(game.active_player) != 0:
            # game is over. Then, return my player's result.
            # find my player
            if maximizing_player:
                my_player = game.active_player
            else:
                my_player = game.get_opponent(game.active_player)
            # return my player result and position.
            return game.utility(my_player), game.get_player_location(my_player)

        # check depth limit.
        assert depth >= 0, f"invalid depth. depth={depth}"
        if depth == 0:
            # depth reached limit. Then, return score.
            # find my player
            if maximizing_player:
                my_player = game.active_player
            else:
                my_player = game.get_opponent(game.active_player)
            # return my player score and position.
            score = self.score(game, my_player)
            return score, game.get_player_location(my_player)

        if maximizing_player:
            # process of function MAX-VALUE
            max_score_and_move = (float("-inf"), (-1, -1))
            for legal_move in game.get_legal_moves(game.active_player):
                next_state = game.forecast_move(legal_move)
                # search minimized score when adversary player to move if search can continue.
                score, _ = self.alphabeta(next_state, depth - 1, alpha, beta, not maximizing_player)
                if score > max_score_and_move[0]:
                    max_score_and_move = (score, legal_move)
                # check alpha-beta pruning
                if score >= beta:
                    return score, legal_move
                # update alpha
                alpha = max(alpha, score)
            return max_score_and_move
        else:
            # process of function MIN-VALUE
            min_score_and_move = (float("inf"), (-1, -1))
            for legal_move in game.get_legal_moves(game.active_player):
                next_state = game.forecast_move(legal_move)
                # search maximized score when my player to move if search can continue.
                score, _ = self.alphabeta(next_state, depth - 1, alpha, beta, not maximizing_player)
                if score < min_score_and_move[0]:
                    min_score_and_move = (score, legal_move)
                # check alpha-beta pruning
                if score <= alpha:
                    return score, legal_move
                # update beta
                beta = min(beta, score)
            return min_score_and_move
