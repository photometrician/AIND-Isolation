# Summary of AlphaGo by the DeepMind Team.
* resource
  * https://storage.googleapis.com/deepmind-media/alphago/AlphaGoNaturePaper.pdf
  
The game of Go has long been viewed as the most challenging of classic games for artificial intelligence owing to its
enormous search space and the difficulty of evaluating board positions and moves. 
To win against top professional players,
the DeepMind Team introduced two types of deep neural networks, 'policy networks' and 'value networks', 
and new search algorithm that combines Monte Carlo simulation with value and policy networks.
I'm going to summarize these technologies and the result below.

The basic concept is how to reduce breadth and depth of search tree.
This is similar to other computer program for games of perfect information.
Breadth of search tree may be reduced by sampling actions from a policy.
The policy is a probability distribution over possible action in a position and 
 used to narrow the search to a high-probability actions.
Depth of search tree may be reduced by position evaluation which replaces subtree into an approximate value.
Monte Carlo tree search (MCTS) is adopted to the search algorithm,
 which uses Monte Carlo rollouts to estimate the value of each state in a search tree.
  Monte Carlo rollouts maximize depth without branching.

To make policy more accurate, AlphaGo uses three policy networks,
 a supervised learning (SL) policy network, a fast policy network, and a reinforcement learning (RL) policy network.   
 The SL policy network is trained directly from expert human moves.
 The SL policy network is in the first stage of the training pipeline.
 The RL policy network is in the second stage.
 The RL policy network is identical in structure to the SL policy network, and its weights' are initialized to same weight of the SL policy network.
 They play games between the current RL policy network and a randomly selected previous iteration of the RL policy network for preventing from overfitting.
 These networks are large and slow for rollouts.
 Therefore, they also trained the faster but less accurate policy network from expert human moves, to be used during rollouts.

To improve position evaluation, AlphaGo uses value networks.
 This is the last stage of the training pipeline.
 The value network is used to estimate the value function for the strongest policy derived from RL policy network.
 This neural network has a similar architecture to the policy network, but outputs a single prediction.
 They train the weights of the value network by regression on state-outcome pairs, using stochastic gradient descent to
 minimize the MSE between the predicted value, and the corresponding outcome.
 To prevent from overfitting, they generated a new self-play data set consisting of 30 million distinct positions,
 each sampled from a separate game. Each game was played between the RL policy network and itself until the game terminated.

To combines the policy and value networks with MCTS, they customized the algorithm of Monte Carlo tree search operation, selection, expansion, evaluation, and backup.

By introducing these new technologies,
 AlphaGoo achieved a 99.8% winning rate against other Go programs,
  and defeated the human European Go champion even though other previous Go programs were amateur level.
