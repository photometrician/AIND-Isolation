# Abstract
ID Improved just counts number of legal moves in a current position and make difference of the each player's.
This is because total number of movable position is unclear at the evaluation time until reaching the end of the game.
 This situation is similar to horizon effect.
I thought it is not enough to detect accurate potential for each player's advantage. 
So I researched two functions, custom_score_movable_num_inner_two_steps and custom_score_movable_num.

And I created two more evaluation functions below.
 * custom_score_movable_num_with_split_check
   * This improves the last phase move by detecting partitions.
 * custom_score_movable_num_with_split_check_after_sticky
   * This avoids horizon effect of partition and has also improvement of custom_score_movable_num_with_split_check.

# Description of each evaluation function.
## custom_score_movable_num_inner_two_steps
This evaluation function searches number of movable position.
 This is similar to ID_Improved.
  But the feature of this is to detect number of position inner two step.

## custom_score_movable_num
This evaluation function is composed of the two phases.
The first phase is same as custom_score_movable_num_inner_two_steps.
And after the number of remaining blank position is lesser than the half of the total positions,
 The phase switches to the second.
 In the second phase, this evaluation function searches all movable positions in the future for each player at the evaluated states, ane makes difference of the number.
 The reason to divide two phases is that the cost of searching all position is high.
  Therefore searching all movable must be used at the last phase of the game.

## custom_score_movable_num_with_split_check
This evaluation function is also composed of the two phases.
And it is same as custom_score_movable_num in the first phase.
 In the last phase, this function check whether partition created at the board.
 If partition created, the difference of total number of movable positions is higher weighted than other situations.

## custom_score_movable_num_with_split_check_after_sticky
This evaluation function is also composed of the two phases.
And it is same as custom_score_movable_num_with_split_check in the last phase.
 I found some games lost by inevitable partition in the last phase.
 Therefore we need to remove to chance to move to disadvantaged partition in the first phase.
 To avoid risky partitions, this method move to near a adversary player in the first phase.
 After switching to the last phase, our player quickly detect partitions and take advantage for partitions.


# The results of performance and which to recommend
To execute tournament.py, I set NUM_MATCHES to 10 for a little more accuracy.

heuristic function | Results(Winning percentage)
-------------|-----
ID_Improved   | 67.14%
custom_score_movable_num_inner_two_steps      | 67.14%
custom_score_movable_num      | 71.21%
custom_score_movable_num_with_split_check | 73.93%
custom_score_movable_num_with_split_check_after_sticky | 78.21%

* custom_score_movable_num_inner_two_steps
  * Performance is same as ID Improved. This seems to be because (pseudo) horizon effect can not be suppressed at most one step precedence compared to ID Improved.
* custom_score_movable_num
  * This method seems to be able to suppress (pseudo) horizon effect at the last phase and more improved than custom_score_movable_num_inner_two_steps.
* custom_score_movable_num_with_split_check
  * This method seems to be able to detect partitions in the last phase and more improved than custom_score_movable_num.
* custom_score_movable_num_with_split_check_after_sticky
  * This method seems to be able to avoid risky partitions in the first phase and move improved than custom_score_movable_num_with_split_check.
  * Compared to ID_Improved, this method has three feature.
    * suppress horizon effect at the last phase.
    * detect partitions at the last phase.
    * avoid risky partitions at the first phase.
  * This method is the best among mine from above features.
  * The not good points are blow.
    * Approaching adversary does not have general effectiveness. Some player who considers partitions might be able to win against this strategy.
    * The number of steps when to last phase is depend on a local environment. So the optimal number should be determined by manual or machine learning.